library;

export 'generated/assets.gen.dart';
export 'generated/translations.g.dart';
export 'src/global/colors.dart';
export 'src/global/controllers/session_controller.dart';
export 'src/global/theme.dart';
export 'src/modules/home/views/home_view.dart';
export 'src/modules/offline/views/offline_view.dart';
export 'src/modules/sign_in/views/sign_in_view.dart';
export 'src/modules/splash/views/splash_view.dart';
export 'src/routes/app_routes.dart';
export 'src/routes/routes.dart';
