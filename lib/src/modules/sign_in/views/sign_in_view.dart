import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../global/widgets/statefulwidgets/password_field.dart';
import 'controller/sign_in_controller.dart';
import 'controller/state/sign_in_state.dart';
import 'form/form.dart';
import 'widgets/submit_button.dart';

class SignInView extends StatelessWidget {
  const SignInView({super.key});

  @override
  Widget build(BuildContext context) {
    final LoginForm loginForm = LoginForm();

    SignInController controllerCreator = SignInController(
      const SignInState(),
      authenticationRepository: context.read(),
    );

    controllerCreator.onForm();

    return ChangeNotifierProvider<SignInController>(
      create: (_) => controllerCreator,
      child: Scaffold(
          body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: ReactiveForm(
              formGroup: controllerCreator.state.form!,
              child: Builder(builder: (context) {
                final controller = Provider.of<SignInController>(
                  context,
                  listen: true,
                );
                return AbsorbPointer(
                    absorbing: controller.state.fetching,
                    child: Column(
                      children: <Widget>[
                        ReactiveTextField<String>(
                            enableInteractiveSelection: true,
                            formControlName: 'username',
                            obscureText: false,
                            decoration:
                                const InputDecoration(labelText: 'Username'),
                            validationMessages: loginForm.usernameMessage),
                        PasswordField(
                          validateMessage: loginForm.passwordMessage,
                          controllerName: 'password',
                          label: 'Password',
                        ),
                        const SizedBox(height: 20),
                        ReactiveFormConsumer(builder: (context, form, child) {
                          return const SubmitButton();
                        }),
                      ],
                    ));
              }),
            ),
          ),
        ),
      )),
    );
  }
}
