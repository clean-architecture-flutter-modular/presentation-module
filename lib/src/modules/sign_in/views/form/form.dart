import 'package:reactive_forms/reactive_forms.dart';

class LoginForm {
  String get patternUserName {
    return r'^[a-zA-Z0-9_-]*$';
  }

  FormGroup get form {
    return FormGroup({
      'username': FormControl<String>(validators: [
        Validators.required,
        Validators.pattern(patternUserName),
        Validators.minLength(3),
        Validators.maxLength(20)
      ], touched: false),
      'password': FormControl<String>(validators: [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)
      ]),
    });
  }

  Map<String, String Function(dynamic)> get usernameMessage {
    return {
      'required': (error) => 'El usuario es requerido',
      'minLength': (error) => 'El usuario debe tener al menos 3 caracteres',
      'maxLength': (error) => 'El usuario debe tener menos de 20 caracteres',
      'pattern': (error) =>
          'El usuario es alfanumerico, guiones y guiones bajos'
    };
  }

  Map<String, String Function(dynamic)> get passwordMessage {
    return {
      'required': (error) => 'La contraseña es requerida',
      'minLength': (error) => 'La contraseña debe tener al menos 8 caracteres',
      'maxLength': (error) => 'La contraseña debe tener menos de 20 caracteres',
    };
  }
}
