import 'package:domain/domain.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../../global/state_notifier.dart';
import '../form/form.dart';
import 'state/sign_in_state.dart';

class SignInController extends StateNotifier<SignInState> {
  SignInController(
    super.state, {
    required this.authenticationRepository,
  });

  final AuthenticationRepository authenticationRepository;

  void onForm() {
    onlyUpdate(
      state.copyWith(
        form: LoginForm().form,
      ),
    );
  }

  Future<Either<SignInFailure, User>> submit() async {
    state = state.copyWith(fetching: true);
    FormGroup form = state.form!;
    String username = form.control('username').value.toString();
    String password = form.control('password').value.toString();

    final result = await authenticationRepository.signIn(
      username,
      password,
    );

    result.when(
      left: (_) => state = state.copyWith(fetching: false),
      right: (user) {
        form.reset();
      },
    );

    return result;
  }
}
