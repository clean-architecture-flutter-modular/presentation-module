import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../routes/routes.dart';
import '../controller/sign_in_controller.dart';

class SubmitButton extends StatefulWidget {
  const SubmitButton({super.key});

  @override
  SubmitButtonState createState() => SubmitButtonState();
}

class SubmitButtonState extends State<SubmitButton> {
  late final SignInController _controller;
  bool _isValid = false;

  @override
  void initState() {
    super.initState();
    _controller = Provider.of<SignInController>(context, listen: false);
    _controller.state.form!.valueChanges.listen((value) {
      setState(() {
        _isValid = _controller.state.form!.valid;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_controller.state.fetching) {
      return const CircularProgressIndicator();
    }

    return _isValid ? formValidButton() : formInValidButton();
  }

  Widget formValidButton() {
    return ElevatedButton(
      onPressed: () {
        _submit(context);
      },
      child: const Text('Login'),
    );
  }

  Widget formInValidButton() {
    return const ElevatedButton(
      onPressed: null,
      child: Text('Login'),
    );
  }

  Future<void> _submit(BuildContext context) async {
    final SignInController controller = context.read();

    final result = await controller.submit();

    if (!controller.mounted) {
      return;
    }

    result.when(
      left: (failure) {
        final message = failure.when(
          unauthorized: () => 'Invalid User name or password',
          invalidUsernameOrPassword: () => 'Invalid User name or password',
          serverError: () => 'Server Error',
          notFound: () => 'Not Found',
          networkError: () => 'Network Error',
          unknown: () => 'Unknown Error',
          notVerified: () => 'Not Verified',
        );

        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(message),
          ),
        );
      },
      right: (_) => Navigator.pushReplacementNamed(
        context,
        Routes.home,
      ),
    );
  }
}
