import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

class PasswordField extends StatefulWidget {
  const PasswordField({
    required this.controllerName,
    required this.label,
    required this.validateMessage,
    super.key,
  });

  final String controllerName;
  final String label;
  final Map<String, String Function(dynamic)> validateMessage;

  @override
  PasswordFieldState createState() => PasswordFieldState();
}

class PasswordFieldState extends State<PasswordField> {
  bool _showText = true;

  void _toggleText() {
    setState(() {
      _showText = !_showText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ReactiveTextField<String>(
        enableInteractiveSelection: true,
        formControlName: widget.controllerName,
        obscureText: _showText,
        decoration: InputDecoration(
          labelText: widget.label,
          suffixIcon: IconButton(
              onPressed: () {
                _toggleText();
              },
              icon: const Icon(Icons.remove_red_eye)),
        ),
        validationMessages: widget.validateMessage);
  }
}
